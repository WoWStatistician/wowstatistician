defmodule WowStatisticianWeb.Router do
  use WowStatisticianWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", WowStatisticianWeb do
    pipe_through :api
  end
end
