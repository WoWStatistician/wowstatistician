# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :wow_statistician,
  ecto_repos: [WowStatistician.Repo]

# Configures the endpoint
config :wow_statistician, WowStatisticianWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "XUb6Fa0O2/SJlCxgTfV0Ir+iibLQSmOLB8U69gPKu1NBycZ/l4mlQuMCgkitoX0t",
  render_errors: [view: WowStatisticianWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: WowStatistician.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
