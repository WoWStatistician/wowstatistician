# WowStatistician

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.create && mix ecto.migrate`
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Ready to run in production? Please [check our deployment guides](http://www.phoenixframework.org/docs/deployment).

## Production database set-up

```sql
CREATE DATABASE wow_statistician_dev_database;

CREATE USER wow_statistician_dev_user WITH ENCRYPTED PASSWORD 'wow_statistician_dev_user';

GRANT ALL PRIVILEGES ON DATABASE wow_statistician_dev_database TO wow_statistician_dev_user;
```

## Learn more

  * Official website: http://www.phoenixframework.org/
  * Guides: http://phoenixframework.org/docs/overview
  * Docs: https://hexdocs.pm/phoenix
  * Mailing list: http://groups.google.com/group/phoenix-talk
  * Source: https://github.com/phoenixframework/phoenix
